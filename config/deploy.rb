# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "handsome"
set :repo_url, "git@gitlab.com:dutch-js/laravel-handsome.git"


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, "master"

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"
set :deploy_to, proc { "/var/www/html/handsome" }

# Default value for :format is :airbrussh.
set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto
set :format_options, command_output: true, log_file: "logs/capistrano-handsome.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
append :linked_files, ".env"

# Default value for linked_dirs is []
append :linked_dirs, "storage", "vendor"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 5

SSHKit.config.command_map[:chmod] = "sudo chmod"
SSHKit.config.command_map[:chown] = "sudo chown"
SSHKit.config.command_map[:supervisord] = "sudo service supervisord"

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure


namespace :deploy do
    before :finished, "composer:install"
    before :finished, "composer:dumpautoload"
    before :finished, "laravel:permission"
    # before :finished, "laravel:migrate"
    # before :finished, "laravel:seed"
    ## before :finished, "node:install"
    ## before :finished, "node:build_production"
    before :finished, "laravel:clear_cache"
    before :finished, "laravel:clear_config"
    before :finished, "laravel:clear_route"
    # before :finished, "laravel:clear_view"

    after :finished, :cleanup
end
