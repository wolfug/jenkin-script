namespace :composer do
    desc "Run composer install task"
    task :install do
        on roles(:web) do
            within release_path do
                execute :composer, "install"
            end
        end
    end

    desc "Run composer update task"
    task :update do
        on roles(:web) do
            within release_path do
                execute :composer, "update"
            end
        end
    end

    desc "Run composer dumpautoload task"
    task :dumpautoload do
        on roles(:web) do
            within release_path do
              execute :composer, "dumpautoload"
            end
        end
    end

end